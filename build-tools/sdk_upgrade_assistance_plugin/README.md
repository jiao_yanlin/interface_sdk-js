# SDK升级辅助工具

## 简介

该工具用于帮助开发者快速解决SDK升级导致的API不兼容问题。
Beta版本不承诺API稳定性，在SDK升级后，可能存在API不兼容的问题，应用开发者对现在工程切换API版本后，需要适配API接口及其底层行为的变更，存在一定的升级成本；因此OpenHarmony提供了SDK升级辅助工具，可以帮助开发者快速了解升级适配全貌，并通过工具提示快速适配升级，显著提高SDK升级效率。

## 使用方式

详情请参考[suap-tool.md](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/tools/suap-tool.md)