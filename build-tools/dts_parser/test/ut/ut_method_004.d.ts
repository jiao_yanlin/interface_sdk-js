/**
 * the ut for method in sourcefile, which has params and return value, one of params is optional
 */
export declare function test(param1: string, param2?: string): number;