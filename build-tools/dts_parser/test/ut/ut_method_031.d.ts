/**
 * the ut for method in interface, which doesn't have params and return value
 */
export interface Test {
  test(): void;
}