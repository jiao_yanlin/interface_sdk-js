/**
 * the ut for method in sourcefile, method has event subscription and type is union type
 */
export declare function on(type: 'play' | 'pause', callback: Callback<void>): void;

/**
 * the ut for method in sourcefile, method has event subscription and type is union type
 */
export declare function off(type: 'play' | 'pause', callback: Callback<void>): void;